import React from 'react';
import { render } from 'react-dom';
import marked from "marked"

import './index.css';
import './style/css/bootstrap.css';
import { sampleText } from './sampleText';


class App extends React.Component {

	state = {
		text: sampleText
	};
    /*
    permet de creer un stokage local,celui ci est mise a jour a
    avant le rendu
    */
    componentWillUpdate(nextProps, nextState){

        localStorage.setItem(
            'text',
            nextState.text
        );
    }

    //
    componentWillMount(){

        const localStorageText = localStorage.getItem('text');
        console.log(localStorageText)
        
        //s'il ya modification,modifier le state
        if(localStorageText){
            this.setState({
                id:localStorageText
            })
        }
    }

    //afficher la valeur a chaque modification
    editText = (event) => {

        const text = event.target.value;
        console.log(text);

        //modifier le state
        this.setState({
            //text:text
            //avec es6 si on a deux moots qui signifie la mm chose on pourra garder qu'un
            text: text
        })
    }

    //on va donner a notre methode du text qu'il va traduire
    renderText = (text) => {

        //on va donner a marke du text qu'il va traduire 
        const textTraduit = marked(
            text, 
            {
                sanitize:true
            }
        );

        return {
            __html : textTraduit
        };
    }
   
	render() {
		return (
			<div className="container">
				<div className="row">

				    <div className="col-sm-6">
                      <textarea
                        value = {this.state.text}
                        rows = "35"
                        className = "form-control"
                        onChange = { (event) => this.editText(event) }>
				      </textarea>
				    </div>

				<div className="col-sm-6">
				  	<div dangerouslySetInnerHTML = {this.renderText(this.state.text)}/>
				</div>

				</div>
			</div>
		)
	}

}

render(
  <App />,
  document.getElementById('root')
);